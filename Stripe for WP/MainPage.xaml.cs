﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace Stripe_for_WP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            ShowAuthSite.Begin();
            button.Content = "Connecting...";
            button.IsEnabled = false;
            authSite.Navigate(new Uri("https://connect.stripe.com/oauth/authorize?response_type=code&client_id=ca_4toSU8hFU5ovApi1GQehMHy9uTAn9OsK&scope=read_write"));
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void authSite_DOMContentLoaded(WebView sender, WebViewDOMContentLoadedEventArgs args)
        {
            loading.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            LogoFadeOut.Begin();
            
        }

        private void ShowAuthSite_Completed(object sender, object e)
        {
            loading.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void LogoFadeOut_Completed(object sender, object e)
        {
            button.Content = "Waiting for auth";
            authSite.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private void authSite_LoadCompleted(object sender, NavigationEventArgs e)
        {
            
            MessageDialog md = new MessageDialog(e.Uri.ToString());
            md.ShowAsync();
        }
    }
}
